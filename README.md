# Symfony and React app dockerized !
This repository is an easy to use dockerization of a symfony 5 api and a react app frontend.

## Getting started
Make sure to have [docker-compose](https://docs.docker.com/compose/) installed locally in order to use this repo.

Once it's done, simply run `docker-compose up --build` on main folder. It will locally creates a ***front*** and a ***back*** folders beside the ***docker*** one already there.

Please, consider that this project has been built with macOS and that I cannot certify it will running well on other platforms.

Also, the first run make take a while as it creates and installs symfony and react.
Wait for these logs to appear to be sure both *Node* and *PHP/Apache* containers are initialized :
![PHP/Apache](https://i.ibb.co/c1Sf4y2/Capture-d-cran-2021-04-23-11-05-46.png)
![Node](https://i.ibb.co/D5KG276/Capture-d-cran-2021-04-23-11-03-55.png)

## Run
For every other times you'd like to run the containers, you should run `docker-compose up` instead of `docker-compose up --build` to prevent rebuilding every time the Docker images.

## What's inside ?
3 containers :
- One *MySQL* database container
  - Based on mariadb:10.5 image
- One *PHP/Apache* container where the symfony project is located
  - Based on alpine:3.13 image
  - All php packages needed to run Symfony 5
  - Custom entrypoint that :
    - Installs Symfony 5 if it hasn't been done yet
    - Configures Apache2's base directory
    - Runs Apache2
- One *Node* container where the react app is located
  - Based on node:12 image
  - Custom entrypoint that :
    - Installs React if it hasn't been done yet
    - Runs `npm install` if project already exists
    - Runs React app

Each containers is mapped on the host's custom port : 
- 3307 for the *MySQL* container
- 81 for the *PHP/Apache* container
- 3001 for the *Node* container

The *MySQL* and the *PHP/Apache* containers share the same virtual network as well as the *PHP/Apache* and the *Node* containers are sharing another virtual network. 
This way, no communication is planned to be made directly between the *Node* container and the *MySQL* one. 
Every bdd request has to pass through the *PHP/Apache* container which main purpose is to be an API.