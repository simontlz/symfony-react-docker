#!/bin/bash
if [ ! -d "./" ] || [ -z "$(ls -A ./)" ]; then
  # The directory doesn't exist or is empty
  composer create-project symfony/website-skeleton .

  # Setting up bdd conf for symfony
  baseDbURL='DATABASE_URL="postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=13&charset=utf8"'
  newDbURL='DATABASE_URL="mysql://symfony_user:symfony_password@db:3306/symfony"'
  sed -i "s|${baseDbURL}|${newDbURL}|g" .env

  # Setting up apache to tarfet the newly created symfony project
  baseDocumentRoot='DocumentRoot "/var/www/localhost/htdocs"'
  newDocumentRoot='DocumentRoot "/var/www/localhost/htdocs/symfony/public"'
  baseDirectory='<Directory "/var/www/localhost/htdocs">'
  newDirectory='<Directory "/var/www/localhost/htdocs/symfony/public">'
  sed -i "s|${baseDocumentRoot}|${newDocumentRoot}|g" /etc/apache2/httpd.conf
  sed -i "s|${baseDirectory}|${newDirectory}|g" /etc/apache2/httpd.conf
fi

/usr/sbin/httpd -D FOREGROUND -f /etc/apache2/httpd.conf