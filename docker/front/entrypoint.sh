#!/bin/bash
if [ ! -d "./" ] || [ -z "$(ls -A ./)" ]; then
  # The directory doesn't exist or is empty
  npx create-react-app .
else
  npm install
fi

npm start